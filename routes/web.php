<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('productList', 'ProductController@productList')->name('productList');
Route::post('addToCart', 'ProductController@addToCart')->name('addToCart');
Route::post('removeFromCart', 'ProductController@removeFromCart')->name('removeFromCart');
Route::get('cart', 'ProductController@cart')->name('cart');


Route::middleware('auth')->group(function () {
    Route::get('payment', 'PaymentController@payment')->name('payment');
    Route::post('purchase', 'PaymentController@purchase')->name('purchase');
});

Route::prefix('admin')->middleware('checkAdmin')->group(function () {
    Route::resource('products', 'ProductController', array('as'=>'admin'));
    Route::get('orderList', 'OrderController@index')->name('orderList');
    Route::post('approve', 'OrderController@approve')->name('approve');
});


Route::get('/home', 'HomeController@index')->name('home');
