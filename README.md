Projeyi klonladıktan sonra aşağıdaki adımları uygulayarak projeyi lokalinizde ayağı kaldırabilirsiniz. Proje de sizin görmeniz
adına ben seed datalar oluşturdum. Bir admin kullanıcısı, kategori, marka, role ve product tablolarını aşağıdaki komutları
uygulayarak kayıtlarla birlikte oluşturabilirsiniz.

Admin kullanıcısı:
adminuser@adminuser.com
Şifre: 123456

-Proje dizinine gidilir.

-composer install => kütüphaneleri yükler.

-npm install && npm run dev => bootsrap vb. paketleri yükler.

-.env.example dosyasını koplayıp .env diye bir dosya yaratılır.

-.env dosyasında, kullandığınız database -konfigürasyonları yapılır.

-php artisan key:generate => yeni bir app key oluşturulur.

Tabloların oluşturulması için sırayla aşağıdaki komutlar çalıştırılır. Sırayla olmasının sebebi de ilişkili tablolar olduğundan dolayıdır.

-php artisan migrate --path=/database/migrations/2020_06_06_142217_create_roles_table.php => Role tablosu yaratılır.

-php artisan migrate --path=/database/migrations/2020_06_06_131125_create_brands_table.php => Brand tablosu yaratılır.

-php artisan migrate --path=/database/migrations/2020_06_06_131208_create_categories_table.php => Category tablosu yaratılır.

-php artisan migrate => Geri kalan tablolar yaratılır.

-php artisan db:seed => Seed datalar oluşturulur.

-php artisan serve => Artisan komutu ile serve edebilirsiniz.

User kullanısını da register'ı kullanarak oluşturabilirsiniz. User kullanıcısı, adminin ulaştığı routelara midlleware sayesinde ulaşamaz.
User home, cart ve product sayfalarını görebiir. Admin ise ürünleri düzenleyebileceği products ve siparişleri görebileceği
orders sayfalarına ulaşabilir. Projede ürünler cookie de tutuldu. Redis de tutulabilir. Loglama kullanmadım, ama loglama kullanılabilir. Bazı tabloları 
yaratmadım, örnek verecek olursam payment_type tablosu. Tek seçenek olduğu için o kısmı atladım. Pazar günü işim çıktığından dolayı bu kadar uğraşabildim, 
daha da güzel olabilirdi. Repository pattern kullandım. Status için enumları kullandım. Controllerlar da kod kalabalığı olacağı durumlarda, servis classları
oluşturup, db vb işlemleri orada yapabilirdik. Validate işlemleri yapılmadı. Order yapısı için iki tablo kullandım. Biri itemları tutan order_items,
diğeri de order'ı tutan orders tablosu. Bir siparişte birden çok ürün olabileceği için böyle bir yapı kullanıldı ve ilişki kuruldu. Gene aynı şekilde
product ile category, product ile brand, order items ile product, order ile user ilişkileri kuruldu. User tabloundaki role_id ile role tablosu arasında
ilişki kuruldu. Middleware'ler bu role_id alanına göre yapılandırıldı.