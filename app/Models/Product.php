<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description', 'price', 'status', 'category_id', 'brand_id'];

    public function orderItem()
    {
        return $this->belongsTo('App\Models\OrderItem', 'product_id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'category_id');
    }

    public function brand()
    {
        return $this->hasOne('App\Models\Brand', 'brand_id');
    }
}
