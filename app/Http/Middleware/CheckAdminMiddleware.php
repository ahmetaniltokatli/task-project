<?php

namespace App\Http\Middleware;

use App\Helpers\UserRoleEnums;
use Closure;

class CheckAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();


        if(!$user)
            return redirect(route('login'));

        if($user->role_id != UserRoleEnums::ADMIN)
            return redirect(route('home'));

        return $next($request);
    }
}
