<?php

namespace App\Http\Controllers;

use App\Helpers\PaymentHelper;
use App\Helpers\ResultEnums;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class PaymentController extends Controller
{
    public function payment()
    {
        $cookies = Cookie::get('products');
        $response = [];
        $quantities = [];
        if($cookies) {
            $response = Product::whereIn('id', array_keys(unserialize($cookies)))->get();
            $quantities = array_values(unserialize($cookies));
        }

        return view('product/payment')->with(['products' => $response, 'quantities' => $quantities]);
    }

    public function purchase(Request $request)
    {
        $helper = new PaymentHelper();
        $cookies = Cookie::get('products');

        try{
            if($cookies) {
                $parseCookie = unserialize($cookies);
                $order = new Order();
                $order->user_id = auth()->user()->id;
                $order->address = $request->address;
                $order->total_amount = $helper->getAmount();
                $order->payment_type = 'cash on delivery'; //it can take from payment type enums or payment type table
                $order->save();

                foreach($parseCookie as $product => $quantity) {
                    $orderItem = new OrderItem();
                    $productDb = Product::where('id', $product)->select('price')->first();
                    $orderItem->order_id = $order->id;
                    $orderItem->product_id = $product;
                    $orderItem->quantity = $quantity;
                    $orderItem->price = $productDb->price;
                    $orderItem->save();
                }
            }
        }catch (\Exception $ex) {
            //log
            return response()->json(['status' => ResultEnums::FAIL]);
        }

        return response()->json(['status' => ResultEnums::SUCCESS]);
    }
}
