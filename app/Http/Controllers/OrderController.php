<?php

namespace App\Http\Controllers;

use App\Helpers\PaymentStatus;
use App\Helpers\ResultEnums;
use App\Models\Order;
use App\Repositories\Repository;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $model;

    public function __construct(Order $order)
    {
        $this->model = new Repository($order);
    }

    public function index()
    {
        return view('order/admin/list')->with('orders', $this->model->all());
    }

    public function approve(Request $request)
    {
        try {
            $order = $this->model->show($request->id);
            $order->status = PaymentStatus::COMPLETED;
            $order->save();
        }catch (\Exception $ex) {
            //log
            return response()->json(['status' => ResultEnums::FAIL]);
        }

        return response()->json(['status' => ResultEnums::SUCCESS]);
    }

}
