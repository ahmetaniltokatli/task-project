<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Repositories\Repository;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $model;

    public function __construct(Product $product)
    {
        $this->model = new Repository($product);
    }

    public function productList()
    {
        return view('product/index')->with('products', $this->model->all());
    }

    public function index()
    {
        return view('product/admin/list')->with('products', $this->model->all());
    }

    public function store(Request $request)
    {
        $this->model->create($request->only($this->model->getModel()->fillable));
        return redirect()->route('admin.products.index')
            ->with('success','Product inserted successfully');
    }

    public function create()
    {
        $categories = Category::all();
        $brands = Brand::all();
        return view('product/admin/create')->with(['categories' => $categories, 'brands' =>$brands]);
    }

    public function edit(Product $product)
    {
        $categories = Category::all();
        $brands = Brand::all();

        return view('product/admin/show')->with(['product' => $this->model->show($product->id), 'categories' => $categories, 'brands' =>$brands]);
    }

    public function update(Request $request, Product $product)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $product->id);

        return redirect()->route('admin.products.index')
            ->with('success','Product updated successfully');
    }

    public function destroy(Product $product)
    {
        try {
            $this->model->delete($product->id);
        }catch (\Exception $ex) {
            return redirect()->route('admin.products.index')
                ->with('fail','Product could not be deleted');
        }

        return redirect()->route('admin.products.index')
            ->with('success','Product deleted successfully');
    }

    public function addToCart(Request $request)
    {
        $cookies = Cookie::get('products');
        if(!$cookies) {
            $cookies = [];
            $cookies[$request->id] = 1;
            Cookie::queue('products', serialize($cookies), 86400);
        }
        else {
            $parseCookies = unserialize($cookies);
            if (array_key_exists($request->id, $parseCookies)) {
                $parseCookies[$request->id] = $parseCookies[$request->id] + 1;
            }else {
                $parseCookies[$request->id] = 1;
            }

            Cookie::queue('products', serialize($parseCookies), 86400);
        }
    }

    public function removeFromCart(Request $request)
    {
        $cookies = Cookie::get('products');
        $parseCookies = unserialize($cookies);
        if (array_key_exists($request->id, $parseCookies)) {
            if($parseCookies[$request->id] != 1)
                $parseCookies[$request->id] = $parseCookies[$request->id] - 1;
            else
                unset($parseCookies[$request->id]);
        }

        Cookie::queue('products', serialize($parseCookies), 86400);

        $cookies = $parseCookies;
        $result = [];

        if($cookies) {
            $response = $this->model->getProductsWithArray(array_keys($cookies));
            $quantities = array_values($cookies);
            $result["response"] = $response;
            $result["quantities"] = $quantities;
        }

        return response()->json($result);
    }

    public function cart()
    {
        $cookies = Cookie::get('products');
        $response = [];
        $quantities = [];
        if($cookies) {
            $response = $this->model->getProductsWithArray(array_keys(unserialize($cookies)));
            $quantities = array_values(unserialize($cookies));
        }

        return view('product/cart')->with(['products' => $response, 'quantities' => $quantities]);
    }

}
