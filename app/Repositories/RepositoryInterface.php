<?php


namespace App\Repositories;


use Illuminate\Http\Request;

interface RepositoryInterface
{
    public function all();

    public function create(array $data);

    public function update(array $data, $id);

    public function getProductsWithArray(array $data);

    public function delete($id);

    public function show($id);
}
