<?php


namespace App\Helpers;


class PaymentStatus
{
    const COMPLETED = 1;
    const PENDING = 2;
    const CANCELLED = 3;
}
