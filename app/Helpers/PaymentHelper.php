<?php

namespace App\Helpers;

use App\Models\Product;
use Illuminate\Support\Facades\Cookie;

class PaymentHelper
{
    public function getAmount() {

        $cookies = Cookie::get('products');
        $parseCookie = unserialize($cookies);
        $sum = 0;
        if($cookies) {
            $products = Product::whereIn('id', array_keys(unserialize($cookies)))->get();
            foreach ($products as $product) {
                $sum = $sum + ($product->price * $parseCookie[$product->id]);
            }
        }

        return $sum;
    }

}
