<?php

namespace App\Helpers;

class StatusEnums {
    const ACTIVE = 1;
    const PASSIVE = 0;
}
