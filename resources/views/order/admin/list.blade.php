@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Orders</span>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">User</th>
                                <th scope="col">Total Amount</th>
                                <th scope="col">Payment Type</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="cartBody">
                            @foreach($orders as $order)
                                <tr>
                                    <th scope="row">{{$order->id}}</th>
                                    <td>{{$order->user_id}}</td>
                                    <td>${{$order->total_amount}}</td>
                                    <td>{{$order->payment_type}}</td>
                                    <td>@php
                                        if($order->status == \App\Helpers\PaymentStatus::COMPLETED)
                                            echo 'Completed';
                                        elseif($order->status == \App\Helpers\PaymentStatus::PENDING)
                                            echo 'Pending';
                                        else
                                            echo 'Cancelled';
                                    @endphp
                                    </td>
                                    <td>
                                        @if($order->status == \App\Helpers\PaymentStatus::PENDING)
                                            <button class="btn btn-large btn-primary" onclick="approve({{$order->id}})">Approve</button>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    function approve(id) {
        $.ajax({

            type:'POST',

            url:'{{ route('approve') }}',

            data:{id: id, "_token": "{{ csrf_token() }}"},

            success:function(data){
                if(data.status == 1){
                    window.location.reload();
                }
            }

        });
    }
</script>
