@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Carts</div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Price</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="cartBody">
                            @if(count($products)>0)
                            @php
                                $sum = 0
                            @endphp
                            @foreach($products as $key => $product)
                                @php
                                    $sum = $sum +  ($product->price * $quantities[$key])
                                @endphp
                            <tr>
                                <th scope="row">{{$product->id}}</th>
                                <td>{{$product->name}}</td>
                                <td>{{$quantities[$key]}}</td>
                                <td>${{$product->price}}</td>
                                <td><button class="btn btn-sm btn-danger" onclick="removeFromCart({{$product->id}})">Remove from Cart</button></td>
                            </tr>
                            @endforeach
                            <tr>
                                <th scope="row"></th>
                                <td></td>
                                <td></td>
                                <td>${{$sum}}</td>
                                <td><button class="btn btn-success" onclick="window.location='{{ route("payment") }}'">PURCHASE</button></td>
                            </tr>
                             @else
                                <tr>
                                    <th scope="row">There is no product</th>
                                </tr>
                             @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    function removeFromCart(id) {
        $.ajax({

            type:'POST',

            url:'{{ route('removeFromCart') }}',

            data:{id: id, "_token": "{{ csrf_token() }}"},

            success:function(data){
                $("#cartBody").empty();
                var sum = 0;
                $.each( data.response, function( key, value ) {
                    sum = sum + (value.price * data.quantities[key]);
                    $("#cartBody").append('<tr><th scope="row">' + value.id + '</th><td>' + value.name + ' </td><td>' + data.quantities[key] + '</td><td>$' + value.price + '</td><td><button class="btn btn-sm btn-danger" onclick="removeFromCart(' + value.id + ')">Remove from Cart</button></td></tr>');
                });
                if(data.response)
                    $("#cartBody").append('<tr><th scope="row"></th><td></td><td></td><td>$' + sum.toFixed(2) + '</td><td><button class="btn btn-success" onclick="window.location=\'{{ route("payment") }}\'">PURCHASE</button></td></tr>');
                else
                    $("#cartBody").append('<tr><th scope="row">There is no product</th></tr>');

            }

        });

    }
</script>
