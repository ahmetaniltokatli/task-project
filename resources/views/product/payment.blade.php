@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Payments</div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Price</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="cartBody">
                            @if(count($products)>0)
                                @php
                                    $sum = 0
                                @endphp
                                @foreach($products as $key => $product)
                                    @php
                                        $sum = $sum +  ($product->price * $quantities[$key])
                                    @endphp
                                    <tr>
                                        <th scope="row">{{$product->id}}</th>
                                        <td>{{$product->name}}</td>
                                        <td>{{$quantities[$key]}}</td>
                                        <td>${{$product->price}}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th scope="row"></th>
                                    <td></td>
                                    <td></td>
                                    <td>${{$sum}}</td>
                                </tr>
                            @else
                                <tr>
                                    <th scope="row">There is no product</th>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label><b>Address</b></label>
                                <textarea id="address" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label><b>Payment Type</b></label><br/>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline1" checked name="customRadioInline1" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadioInline1">Cash on Delivery</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-lg btn-success" onclick="purchase()">PURCHASE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    function purchase() {
        $.ajax({

            type:'POST',

            url:'{{ route('purchase') }}',

            data:{address: $("#address").val(), "_token": "{{ csrf_token() }}"},

            success:function(data){
                if(data.status = 1) {
                    alert('Purchase is success');
                    window.location.href= '{{route('productList')}}';
                }else {
                    alert('Purchase is fail');
                }

            }

        });

    }
</script>
