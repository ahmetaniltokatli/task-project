@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Products</div>

                    <div class="card-body">
                       @foreach($products as $product)
                            <div class="col-md-4 card float-left" style="width: 18rem;height: 350px">
                                <div class="card-body">
                                    <h5 class="card-title">{{$product->name}}</h5>
                                    <p class="card-text">{{$product->description}}<br/><strong>${{$product->price }}</strong></p>
                                    <button class="btn btn-primary" onclick="addToCart({{$product->id}})">Add to Cart</button>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    function addToCart(id) {
        $.ajax({

            type:'POST',

            url:'{{ route('addToCart') }}',

            data:{id: id, "_token": "{{ csrf_token() }}"},

            success:function(data){

                alert("Product added to cart");

            }

        });

    }
</script>
