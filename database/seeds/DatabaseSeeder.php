<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(BrandSeeder::class);
        factory(\App\Models\Product::class, 10)->create();
        factory(\App\User::class)->create();
    }
}
