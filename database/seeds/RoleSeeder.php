<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Admin
        $role = new Role();
        $role->name = 'admin';
        $role->save();

        //User
        $role = new Role();
        $role->name = 'user';
        $role->save();
    }
}
