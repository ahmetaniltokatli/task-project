<?php

use Illuminate\Database\Seeder;
use App\Models\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Admin
        $role = new Brand();
        $role->name = 'Brand 1';
        $role->save();

        //User
        $role = new Brand();
        $role->name = 'Brand 2';
        $role->save();
    }
}
