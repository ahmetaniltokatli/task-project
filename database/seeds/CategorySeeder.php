<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Admin
        $role = new Category();
        $role->name = 'Shoes';
        $role->save();

        //User
        $role = new Category();
        $role->name = 'Dress';
        $role->save();
    }
}
