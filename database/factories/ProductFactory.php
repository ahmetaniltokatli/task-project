<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;
use App\Helpers\StatusEnums;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(3),
        'description' => $faker->paragraph(3),
        'price' => mt_rand(10,100) / 10,
        'category_id' => mt_rand(1,2),
        'brand_id' => mt_rand(1,2),
        'status' => StatusEnums::ACTIVE,
    ];
});
